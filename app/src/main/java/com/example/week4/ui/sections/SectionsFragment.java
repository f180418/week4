package com.example.week4.ui.sections;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.week4.R;
import com.example.week4.databinding.FragmentSectionsBinding;

public class SectionsFragment extends Fragment {

    private FragmentSectionsBinding binding;

    // Add RecyclerView member
    private RecyclerView recyclerView;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sections, container, false);

        return view;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}